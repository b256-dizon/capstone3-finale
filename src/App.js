import './App.css';
import { Container } from 'react-bootstrap';
import {Navigate, BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Home from './Pages/Home';
import AppNavBar from './Component/AppNavBar';

function App() {
  return (

      <Router>
      <AppNavBar/>
        <Container>
          <Routes>
            <Route path= "/" element= {<Home />} />
          </Routes>
        </Container>
      </Router>
 
  );
}

export default App;
